package com.devcamp.country.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.country.models.Country;
import com.devcamp.country.services.CountryService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CountryController {
    @Autowired
    private final CountryService countryService;

    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("/country")
    public ResponseEntity<List<Country>> getAllCountry(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            List<Country> countryList = countryService.getAllCountry(page, size);
            return new ResponseEntity<>(countryList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country/{id}")
    public ResponseEntity<Country> getCountryById(@PathVariable(value = "id") long id) {
        try {
            Country country = countryService.getCountryById(id);
            return new ResponseEntity<>(country, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country/{countryCode}")
    public ResponseEntity<Country> getCountryByCode(@PathVariable(value = "countryCode") String countryCode) {
        try {
            Country country = countryService.getCountryByCountryCode(countryCode);
            return new ResponseEntity<>(country, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/country")
    public ResponseEntity<Country> createCountry(@RequestBody Country country) {
        try {
            Country newCountry = countryService.createCountry(country);
            return new ResponseEntity<>(newCountry, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/country/{id}")
    public ResponseEntity<Country> updateCountry(@PathVariable(value = "id") long id, @RequestBody Country country) {
        try {
            Country newCountry = countryService.updateCountry(id, country);
            return new ResponseEntity<>(newCountry, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/country/{id}")
    public ResponseEntity<Country> deleteCountryById(@PathVariable(value = "id") long id) {
        try {
            countryService.deleteCountry(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country/count")
    public ResponseEntity<Long> countCountry() {
        try {
            long count = countryService.countCountry();
            return new ResponseEntity<>(count, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country/check/{id}")
    public ResponseEntity<Boolean> checkCountryById(@PathVariable(value = "id") Long id) {
        try {
            boolean exists = countryService.checkCountryById(id);
            return new ResponseEntity<>(exists, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
