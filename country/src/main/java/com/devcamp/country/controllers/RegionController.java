package com.devcamp.country.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.country.models.Region;
import com.devcamp.country.services.RegionService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class RegionController {
    private final RegionService regionService;

    public RegionController(RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping("/region")
    public ResponseEntity<List<Region>> getAllRegion(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            List<Region> regionList = regionService.getAllRegions(page, size);
            return new ResponseEntity<>(regionList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/region/{id}")
    public ResponseEntity<Region> getRegionById(@PathVariable(value = "id") long id) {
        try {
            Region region = regionService.getRegionById(id);
            return new ResponseEntity<>(region, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/region/{countryId}/country")
    public ResponseEntity<List<Region>> getRegionsByCountryId(
            @PathVariable(value = "countryId") long countryId) {
        try {
            List<Region> regionList = regionService.getRegionsByCountryId(countryId);
            return new ResponseEntity<>(regionList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/region")
    public ResponseEntity<Region> createRegion(@RequestBody Region region) {
        try {
            Region newRegion = regionService.createRegion(region);
            return new ResponseEntity<>(newRegion, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/region/{id}")
    public ResponseEntity<Region> updateRegion(@PathVariable(value = "id") long id, @RequestBody Region region) {
        try {
            Region newRegion = regionService.updateRegion(id, region);
            return new ResponseEntity<>(newRegion, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/region/{id}")
    public ResponseEntity<Void> deleteRegionById(@PathVariable(value = "id") long id) {
        try {
            regionService.deleteRegion(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/region/{countryId}/count")
    public ResponseEntity<Long> countRegionByCountryId(@PathVariable(value = "countryId") long countryId) {
        try {
            long count = regionService.countRegionByCountryId(countryId);
            return new ResponseEntity<>(count, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/region/{countryId}/check")
    public ResponseEntity<Boolean> checkRegionByCountryId(@PathVariable(value = "countryId") long countryId) {
        try {
            boolean exists = regionService.checkRegionByCountryId(countryId);
            return new ResponseEntity<>(exists, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
