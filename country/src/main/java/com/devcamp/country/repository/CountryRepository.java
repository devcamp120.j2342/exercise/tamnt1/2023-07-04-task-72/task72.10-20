package com.devcamp.country.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.country.models.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

    Country findByCountryCode(String countryCode);

    long countCountry();

    boolean checkCountryById(Long id);
}
