package com.devcamp.country.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.country.models.Country;
import com.devcamp.country.repository.CountryRepository;

@Service
public class CountryService {
    private final CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public List<Country> getAllCountry(int page, int size) {

        Pageable pageable = PageRequest.of(page, size);
        Page<Country> countryPage = countryRepository.findAll(pageable);
        return countryPage.getContent();
    }

    public Country getCountryById(long id) {
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if (optionalCountry.isPresent()) {
            return optionalCountry.get();
        } else {
            return null;
        }
    }

    public Country getCountryByCountryCode(String countryCode) {
        return countryRepository.findByCountryCode(countryCode);

    }

    public Country createCountry(Country country) {
        return countryRepository.save(country);
    }

    public Country updateCountry(long id, Country country) {
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if (optionalCountry.isPresent()) {
            Country existingCountry = optionalCountry.get();
            existingCountry.setCountryCode(country.getCountryCode());
            existingCountry.setCountryName(country.getCountryName());
            existingCountry.setRegions(country.getRegions());
            return countryRepository.save(existingCountry);

        } else {
            return null;
        }

    }

    public void deleteCountry(long id) {
        countryRepository.deleteById(id);
    }

    public long countCountry() {
        return countryRepository.count();
    }

    public boolean checkCountryById(Long id) {
        return countryRepository.existsById(id);
    }

}
